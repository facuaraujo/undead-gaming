<div class="edgtf-separator-holder clearfix <?php echo esc_attr($holder_classes); ?>">
	<div class="edgtf-separator" <?php echo overworld_edge_get_inline_style($holder_styles); ?>></div>
</div>
