<?php

define( 'OVERWORLD_CORE_VERSION', '1.0.1' );
define( 'OVERWORLD_CORE_ABS_PATH', dirname( __FILE__ ) );
define( 'OVERWORLD_CORE_REL_PATH', dirname( plugin_basename( __FILE__ ) ) );
define( 'OVERWORLD_CORE_URL_PATH', plugin_dir_url( __FILE__ ) );
define( 'OVERWORLD_CORE_ASSETS_PATH', OVERWORLD_CORE_ABS_PATH . '/assets' );
define( 'OVERWORLD_CORE_ASSETS_URL_PATH', OVERWORLD_CORE_URL_PATH . 'assets' );
define( 'OVERWORLD_CORE_CPT_PATH', OVERWORLD_CORE_ABS_PATH . '/post-types' );
define( 'OVERWORLD_CORE_CPT_URL_PATH', OVERWORLD_CORE_URL_PATH . 'post-types' );
define( 'OVERWORLD_CORE_SHORTCODES_PATH', OVERWORLD_CORE_ABS_PATH . '/shortcodes' );
define( 'OVERWORLD_CORE_SHORTCODES_URL_PATH', OVERWORLD_CORE_URL_PATH . 'shortcodes' );