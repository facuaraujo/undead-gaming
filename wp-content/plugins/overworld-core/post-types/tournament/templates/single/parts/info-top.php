<div class="edgtf-tournament-single-info-top">
	<div class="edgtf-tournament-single-info-top-inner">
		<?php echo get_the_post_thumbnail(); ?>
		<div class="edgtf-tournament-single-info-top-details">
			<h2 itemprop="name" class="edgtf-name entry-title">
				<?php the_title(); ?>
			</h2>
		</div>
	</div>
</div>