<div class="edgtf-player edgtf-item-space">
    <div class="edgtf-player-inner">
        <?php if (get_the_post_thumbnail($player_id) !== '') { ?>
        <div class="edgtf-player-image">
            <?php echo get_the_post_thumbnail($player_id); ?>
            <div class="edgtf-player-info">
                <?php if (!empty($nickname)) { ?>
                <h4 class="edgtf-player-name entry-title"><?php echo esc_html($nickname); ?></h4>
                <?php } ?>
                <p itemprop="name" class="edgtf-player-nickname"><?php echo esc_html(get_the_title($player_id)); ?>
                </p>
                <span class="u-player-social-icons">
                    <?php foreach ( $player_social_icons as $social_icon ) { ?>
                    <span class="edgtf-player-icon u-icon"><?php echo wp_kses_post( $social_icon ); ?></span>
                    <?php } ?>
                </span>
            </div>
        </div>
        <?php } ?>
    </div>
</div>