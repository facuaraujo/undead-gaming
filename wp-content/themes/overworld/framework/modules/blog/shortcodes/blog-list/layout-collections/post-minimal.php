<li class="edgtf-bl-item edgtf-item-space clearfix">
	<div class="edgtf-bli-inner">
		<div class="edgtf-bli-content">
			<?php overworld_edge_get_module_template_part( 'templates/parts/title', 'blog', '', $params ); ?>

            <div class="edgtf-bli-info">
                <?php overworld_edge_get_module_template_part( 'templates/parts/post-info/date', 'blog', '', $params );?>
            </div>

		</div>
	</div>
</li>