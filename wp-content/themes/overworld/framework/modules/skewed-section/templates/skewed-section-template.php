<div class="edgtf-skewed-section-effect <?php echo esc_attr( $additional_class ) ?>" <?php overworld_edge_inline_style( $skewed_section_effect_style ) ?>>
	<?php echo overworld_edge_get_module_part( $skewed_section_svg ); ?>
</div>