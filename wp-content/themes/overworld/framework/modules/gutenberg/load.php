<?php
if ( overworld_edge_is_plugin_installed( 'gutenberg-editor' ) || overworld_edge_is_plugin_installed( 'gutenberg-plugin' ) ) {
	include_once OVERWORLD_EDGE_FRAMEWORK_MODULES_ROOT_DIR . '/gutenberg/functions.php';
}
