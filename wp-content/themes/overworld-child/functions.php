<?php

/*** Child Theme Function  ***/

if ( ! function_exists( 'overworld_edge_child_theme_enqueue_scripts' ) ) {
	function overworld_edge_child_theme_enqueue_scripts() {
		$parent_style = 'overworld-edge-default-style';
		
		wp_enqueue_style( 'overworld-edge-child-style', get_stylesheet_directory_uri() . '/style.css', array( $parent_style ) );
		wp_enqueue_script( 'custom-js', get_stylesheet_directory_uri() . '/js/custom.js', array( 'jquery' ),'',true );
	}
	
	add_action( 'wp_enqueue_scripts', 'overworld_edge_child_theme_enqueue_scripts' );
}

// INSERT PAGE LOADER AFTER BODY TAG
function custom_content_after_body_open_tag() {
	echo ("
		<div class='u-page-loader_wrapper' id='u-page-loader'>
			<script>
				document.onreadystatechange = function () {
					if (document.readyState === 'complete') {
						const pageLoader = document.getElementById('u-page-loader');
						pageLoader.style.opacity = '0';
						setTimeout(function() {
							pageLoader.style.display = 'none';
						}, 1000);
					}
				};
				
			</script>
		</div>
	");
}

add_action('overworld_edge_action_after_opening_body_tag', 'custom_content_after_body_open_tag');